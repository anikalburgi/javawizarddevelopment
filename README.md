# javawizarddevelopment
Reduced SWING coding. New UI components. Easy layout management.

Please refer documentation in the "doc\" directory.

Note:

This project has been developed back in 2012 when I had just started to know what is computer programming. It is NOT up to date as per latest enhancements in SWING.

The coding standards were not followed and only focus was to learn SWING, Java and complete the desired goal of the project which has actually been achieved.

As there's no license or restriction, you are free to grab the key idea behind this and implement your own project using existing artifacts or do whatever you want with it, but then I'd love to hear little about your further experimentation, that would be great.

With evolution in HTML5, CSS3 & Javascript technologies, I noticed SWING was on its way to extinction and I preferred to abandon further enhancement of this project. Later on my focus totally got shifted to back-end development and I enjoy it much more than UI development.

Thanks!